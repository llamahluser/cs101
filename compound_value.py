import locale
locale.setlocale( locale.LC_ALL, 'English_United States.1252' )

monthsToSave = 12
print("Months to Save:", monthsToSave)

monthlySavingsAmount = 100
print("Monthly Amount to Save is: $", monthlySavingsAmount)

annualInterestRate = 5
print("Annual Percentage Rate is:", annualInterestRate, "%")

monthlyInterestRate = annualInterestRate / 100 / 12
print("Monthly Interest Rate is:", monthlyInterestRate, "%")

MonthlyValue =  monthlySavingsAmount + (monthlyInterestRate * monthlySavingsAmount)

TotalValue = MonthlyValue * monthsToSave

print("Total Amount Saved After", monthsToSave, "Months is $", locale.currency ( TotalValue, grouping = True ))